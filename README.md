# springcloud-idea-chapter06

#### 介绍
 **微服务第六章Zuul  P8** 3

注意修改 yml 文件，使用  

```
mylocalhost:
  ipaddr: 172.16.74.69
eureka:
     client:
        service-url:
           defaultZone: http://txyx.feng4yu.cn:7000/eureka/
     instance:
        hostname: localhost
        prefer-ip-address: true
        instance-id: ${mylocalhost.ipaddr}:${server.port}
```
> 把172.16.74.69 修改为和本机一致