package com.itheima.eurekaprovider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by itcast on 2019/10/23.
 */
@RestController
public class HystrixController {
    @Value("${mylocalhost.ipaddr}")
    String port;
    @RequestMapping("/hi")
    public String hi(String id){
        return "hi,访问成功,IP=" + port + " id= "+id;
    }

}
